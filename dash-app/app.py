# Common imports
import os
import re
import datetime
import pandas as pd

# NLTK
import nltk
nltk.download('punkt')
nltk.download('stopwords')
from nltk.probability import FreqDist
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

# App imports
import credentials
import settings

# PostgreSQL
import psycopg2

# Plotly
import plotly.graph_objs as go

# Dash imports
import dash
import dash_core_components as dcc
import dash_html_components as html


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
app.title = 'Real-Time Twitter Monitor'

server = app.server

app.layout = html.Div(
    [
        # Header
        html.Div(
            [          
                html.H1('Real-Time Twitter Monitor'),

                html.P(id='timenow', className='last-update')
            ],
            className="app-header"
        ),

        # App description
        html.Div(
            [
                dcc.Markdown(
                    """
                    Ce dashboard fournit un rapport en temps réel sur les grèves ayant actuellement lieu en France.
                    
                    Code disponible sur [GitLab](https://gitlab.com/Ejoz/twitter-analysis), application réalisée par [Chloé Laurent](https://www.linkedin.com/in/chlolaurent/)
                    """
                )
            ]
        ),

        # Content
        html.Div(
            [
                # Line chart
                html.Div(
                    [
                        dcc.Graph(id='linechart')
                    ],
                    className="app-linechart"
                ),

                # Tweets display
                html.Div(
                    [
                        html.Div(id="tweet-output")
                    ],
                    className="app-tweet-output"
                ),

                # Bar chart 
                html.Div(
                    [
                        dcc.Graph(id='barchart')
                    ],
                    className="app-barchart"
                ),

                # Pie plot
                html.Div(
                    [
                        dcc.Graph(id='pieplot'),      
                    ],
                    className="app-pieplot"
                )
            ],
            className="app-content"
        ),

        # Footer
        html.Div(
            [
                html.Div(
                    [
                        dcc.Markdown(
                            """
                            Source: [Twitter] (https://twitter.com/)
                            """
                        ),
                    ],
                    className='twitter'
                ),
                html.Div(
                    [
                        dcc.Markdown(
                            """
                            Made with: [Dash](https://dash.plot.ly/)
                            """
                        ),
                    ],
                    className='dash'
                ),
                html.Div(
                    [
                        dcc.Markdown(
                            """
                            Deployed on: [Heroku](https://www.heroku.com/)
                            """
                        ),
                    ],
                    className='heroku'
                )
            ],
            className="app-footer"
        ),

        # Timer for updating the data every 10 seconds
        html.Div(
            [
                dcc.Interval(
                    id='interval-component',
                    interval=settings.GRAPH_INTERVAL,
                    n_intervals=0
                )
            ],
            className="interval"
        )      
    ],
    className="app-layout"
)


def loading_data_from_heroku_postgresql():

    """
    Connect to Heroku PostgreSQL Database
    Query the database
    Returns a dataframe
    """

    # Time
    timenow = (datetime.datetime.now() - datetime.timedelta(hours=1, minutes=0)).strftime('%Y-%m-%d %H:%M:%S')

    # Connecting and querying the DB
    DATABASE_URL = credentials.DB_URI
    connection = psycopg2.connect(DATABASE_URL, sslmode="require")
    cursor = connection.cursor()

    query = """SELECT id_tweet, tweet, created_at, polarity, user_location
               FROM {} WHERE created_at >= '{}'""".format(settings.TABLE_NAME, timenow)

    return pd.read_sql(query, con=connection)


@app.callback(dash.dependencies.Output('timenow', 'children'),
             [dash.dependencies.Input('interval-component', 'n_intervals')])
def get_time(n_intervals):

    """
    Returns time of the update
    Input: settings.GRAPH_INTERVAL
    Output: children in 'app-header'
    """

    timenow = datetime.datetime.now() + datetime.timedelta(hours=1)

    return timenow.strftime("Dernière MAJ: %d %b %Y, %Hh%M")


# Line Chart
@app.callback(dash.dependencies.Output('linechart', 'figure'),
             [dash.dependencies.Input('interval-component', 'n_intervals')])
def update_linechart(n_intervals):

    """
    Updates line chart on the first row on every ten seconds
    Input: settings.GRAPH_INTERVAL
    Output: figure in dcc.Graph
    """

    # Loading data from Heroku PostgreSQL
    df = loading_data_from_heroku_postgresql()

    # Cleaning and preprocessing the data
    df['created_at'] = pd.to_datetime(df['created_at'])
    result = df.groupby([pd.Grouper(key='created_at', freq='2s'), 'polarity']).count()\
             .unstack(fill_value=0).stack().reset_index()
    result = result.rename(columns={"id_tweet":"Number of '{}' mentions".format(settings.TRACK_WORDS),\
             "created_at":"Time in UTC" })

    # Variables creation
    time_series = result["Time in UTC"][result['polarity']==0].reset_index(drop=True)
    x = time_series
    y1 = result["Number of '{}' mentions".format(settings.TRACK_WORDS)][result['polarity']==0].reset_index(drop=True)
    y2 = result["Number of '{}' mentions".format(settings.TRACK_WORDS)][result['polarity']==-1].reset_index(drop=True).apply(lambda x: -x)
    y3 = result["Number of '{}' mentions".format(settings.TRACK_WORDS)][result['polarity']==1].reset_index(drop=True)

    # Graph
    trace1 = dict(
        type="scatter",
        x=time_series,
        y=y1,
        name='Neutral',
        marker=dict(color=settings.NEUTRAL_COLOR),
        fill='tozeroy'
    )

    trace2 = dict(
        type="scatter",
        x=time_series,
        y=y2,
        name='Negative',
        marker=dict(color=settings.NEGATIVE_COLOR),
        fill='tozeroy'
    )

    trace3 = dict(
        type="scatter",
        x=time_series,
        y=y3,
        name='Positive',
        marker=dict(color=settings.POSITIVE_COLOR),
        fill='tozeroy'
    )
    
    layout = dict(
        title='Analyse de sentiments sur les tweets concernant la réforme des retraites',
        showlegend=True
    )

    return dict(data=[trace1, trace2, trace3], layout=layout)


# Bar chart
@app.callback(dash.dependencies.Output('barchart', 'figure'),
             [dash.dependencies.Input('interval-component', 'n_intervals')])
def update_barchart(n_intervals):

    """
    Updates bar char on every ten seconds.
    Input: settings.GRAPH_INTERVAL
    Output: figure in dcc.Graph
    """

    # Loading data from Heroku PostgreSQL
    df = loading_data_from_heroku_postgresql()    

    # Processing the text content
    content = ' '.join(df["tweet"])
    content = re.sub(r"http\S+", "", content)
    content = content.replace('RT ', ' ').replace('&amp;', 'and')
    content = re.sub('[^A-Za-zÀ-ÖØ-öø-ÿ-]+', ' ', content)
    content = content.lower()
    tokenized_word = word_tokenize(content)
    stop_words=set(stopwords.words())
    filtered_sent=[]

    for w in tokenized_word:
        if w not in stop_words:
            filtered_sent.append(w)

    fdist = FreqDist(filtered_sent)
    fd = pd.DataFrame(fdist.most_common(10),
                      columns = ["Word","Frequency"]).drop([0]).reindex()

    data = dict(
        type="bar",
        x=fd["Frequency"],
            y=fd["Word"],
            name="Words",
            marker=dict(color=settings.BAR_COLOR),
            orientation='h'
    )

    layout = dict(
        title='Mots tweetés le plus fréquemment',
        showlegend=False
    )

    return dict(data=[data], layout=layout)


# Pie plot
@app.callback(dash.dependencies.Output('pieplot', 'figure'),
             [dash.dependencies.Input('interval-component', 'n_intervals')])
def update_pieplot(n_intervals):

    """
    Updates pie plot every ten seconds
    Input: settings.GRAPH_INTERVAL
    Output: figure in dcc.Graph
    """

    # Loading data from Heroku PostgreSQL
    df = loading_data_from_heroku_postgresql()

    # Number of tweets according to polarity
    neutral = df['polarity'][df['polarity']==0].count()
    negative = df['polarity'][df['polarity']==-1].count()
    positive = df['polarity'][df['polarity']==1].count()

    # Pie plot
    data = dict(
        type="pie",
        labels=['Neutral', 'Negative', 'Positive'],
        values=[neutral, negative, positive],
        marker=dict(colors=[settings.NEUTRAL_COLOR, settings.NEGATIVE_COLOR, settings.POSITIVE_COLOR]),
        hole=.7,
    )

    layout = dict(
        title='Nombre de tweets postés en une heure',
        showlegend=False,
        annotations=
        [
            dict(
            text="{}".format((neutral+negative+positive)),
            font=dict(size=40),
            showarrow=False
            )
        ]
    )

    return dict(data=[data], layout=layout)


# Display tweets in "real time"
@app.callback(
    dash.dependencies.Output(component_id='tweet-output', component_property='children'),
    [dash.dependencies.Input(component_id='interval-component', component_property='n_intervals')]
)
def update_tweet(n_intervals):

    """
    Updates tweets every ten seconds
    Input: settings.GRAPH_INTERVAL
    Output: children in html.Div
    """

    df = loading_data_from_heroku_postgresql()
    tweet = df.iloc[-1]['tweet']

    return "{}".format(tweet)


if __name__ == '__main__':
    app.run_server(debug=True)
