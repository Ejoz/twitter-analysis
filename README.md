# Twitter Analysis

You can learn a lot from twitter these days, if you know how to analyze the data.
This repository is freely inspired by the articles on Medium from Chulong Li about brand improvement and trend recognition.

You can check the demo [here](https://real-time-twitter-monitor.herokuapp.com/)


If you want to run the code for yourself, please add a `credentials.py` file with your Twitter Developer Account keys, and connect to your MySQL database with your own password.

You can modify the file `settings.py` as you wish to follow any social event or brand you want.

![Dashboard](static/dashboard_twitter_app.gif)

## To run the code for yourself in Jupyter notebook
1. Add a `credentials.py` file with your own Twitter Developer account keys (you can apply [here](https://developer.twitter.com/en/apply-for-access))
```
API_KEY = 'YOUR_API_KEY'
API_KEY_SECRET = 'YOUR_API_KEY_SECRET'
ACCESS_TOKEN = 'YOUR_ACCESS_TOKEN'
ACCESS_TOKEN_SECRET = 'YOUR_ACCESS_TOKEN_SECRET'
```
2. Modify [CollectingDataFromTwitter.ipynb](CollectingDataFromTwiiter.ipynb) to connect it to your own MySQL database
3. Run [CollectingDataFromTwitter.ipynb](CollectingDataFromTwiiter.ipynb) (this file has to run to collect data before you try to analyze it ;))
4. Run [AnalyzingData.ipynb](AnalyzingData.ipynb) to see simple vizualisations
5. Run [BatchAnalysis.ipynb](BatchAnalysis.ipynb) to have a simple dashboard with updates every 60 seconds
